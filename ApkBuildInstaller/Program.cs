﻿using ApkBuildInstaller.ExtensionMethods;

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ApkBuildInstaller
{
    namespace ExtensionMethods
    {
        public static class ProcessStartInfoExtension
        {
            public static void Run(this ProcessStartInfo si, string args)
            {
                si.Arguments = args;
                using (Process exeProcess = Process.Start(si))
                {
                    exeProcess.WaitForExit();
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length != 1)
            {
                Console.WriteLine(_usageHint);
                return;
            }

            var input = args[0].ToLower();

            if(!input.EndsWith(".apk"))
            {
                Console.WriteLine("Input should be .apk");
                return;
            }

            var apkFull = Path.GetFullPath(input);
            var rawName = Path.GetFileNameWithoutExtension(apkFull);

            var nameParts = rawName.Split(new char[] { '.' });
            var name = string.Empty;
            var version = string.Empty;
            var nameCompleted = false;

            Console.WriteLine($"parsing apk name - {rawName}");

            name += nameParts[0];
            nameParts = nameParts.Skip(1).ToArray();
            foreach(var p in nameParts)
            {
                if(!nameCompleted)
                {
                    if(int.TryParse(p, out int _))
                    {
                        version += $"{p}";
                        nameCompleted = true;
                    }else
                    {
                        name += $".{p}";
                    }
                }else
                {
                    if (int.TryParse(p, out int _))
                    {
                        version += $".{p}";
                    }
                    else
                    {
                        Console.WriteLine($"Wrong apk filename format");
                    }
                }
            }

            Console.WriteLine($"name - {name}");
            Console.WriteLine($"version - {version}");

            var obbFiles = Directory.GetFiles(Path.GetDirectoryName(apkFull)).Where(f => f.ToLower().Contains($"{name}.obb")).ToList();

            var si = new ProcessStartInfo();
            si.CreateNoWindow = true;
            si.UseShellExecute = false;
            si.RedirectStandardOutput = true;
            si.RedirectStandardError = true;
            si.RedirectStandardInput = true;
            si.FileName = "adb";

            si.Run($" uninstall {name}");
            Console.WriteLine($"Uninstall {name}");

            si.Run($" install {apkFull}");
            Console.WriteLine($"Install {apkFull}");

            var obbDest = $"/mnt/sdcard/Android/obb/{name}/";

            si.Run($" shell mkdir {obbDest}");
            Console.WriteLine($"Create dir {obbDest}");

            foreach (var f in obbFiles)
            {
                si.Run($" push {f} {obbDest}/");
                Console.WriteLine($"Pushing {f} to {obbDest}/");
            }

            Console.WriteLine("Done!");

            Console.ReadKey();

        }

        private const string _usageHint = "Usage:\n\rApkBuildInstaller (*.apk)";
    }
}
